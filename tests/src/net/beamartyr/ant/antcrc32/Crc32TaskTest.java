/**
 * 
 */
package net.beamartyr.ant.antcrc32;

import static org.junit.Assert.*;

import org.apache.tools.ant.BuildFileTest;
import org.junit.Before;
import org.junit.Test;

/**
 * @author issac
 *
 */
public class Crc32TaskTest extends BuildFileTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		configureProject("build.xml");
	}

	@Test
	public void testString() {
		executeTarget("use.string");
		expectLog("use.string", "Hello, 4223024711!");
	}

}
