# ANT CRC32

Library for calculating CRC32 hashes in ANT

## Usage

	<!-- Ensure that the JAR is in your classpath -->
	<taskdef resource="ant-crc32.xml" />
	...
	<crc32 string="World" property="MyProp" />
	<echo>Hello, ${MyProp}</echo>
   
 Will output:
 
   [echo] Hello, 4223024711! 