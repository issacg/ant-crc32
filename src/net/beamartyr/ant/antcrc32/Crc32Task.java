package net.beamartyr.ant.antcrc32;

import java.util.zip.CRC32;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;

/**
 * @author issac
 * 
 * Main task to calculate a CRC32 checksum.
 * 
 * Example of usage:
 * <pre>
 * {@code
 *   <crc32 string="World" property="MyProp" />
 *   <echo>Hello, ${MyProp}</echo> 
 * }
 * </pre>
 * Will output "[echo] Hello, 4223024711!"
 */
public class Crc32Task extends Task {
	/**
	 * Property to save calulated CRC32 to
	 */
	protected String propName;
	/**
	 * Setter for property
	 * @param property
	 */
	public void setProperty(String property) {
		this.propName = property;
	}
	/**
	 * Raw string to calculate CRC32
	 */
	protected String aString;
	
	/**
	 * Setter for String
	 * @param aString
	 */
	public void setString(String aString) {
		this.aString = aString;
	}
	
	public void execute() {
		try {
			CRC32 crc = new CRC32();
			crc.update(this.aString.getBytes());
			getProject().setProperty(this.propName, String.valueOf(crc.getValue()));
		} catch (Exception e) {
			throw new BuildException(e.getMessage(), e);
		}
	}
}
